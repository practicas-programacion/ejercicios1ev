package unidad3;

import java.util.Scanner;

public class ParImpar {

	public static void main(String[] args) {
		
		int numero = 1;
		
		Scanner in = new Scanner(System.in);
		
		
		while(numero != 0) {
			
			Print("Introduce un número entero (0 para salir): ",false);
			numero = in.nextInt();
			
			if(numero != 0) {
				if(numero % 2 == 0)
					Print("Es par",true);
				else
					Print("Es impar",true);
			}
			else
				Print("Gracias",false);
			
		}
		
		in.close();

	}
	
	private static void Print(String cadena, Boolean ln) {
		
		if(ln)
			System.out.println(cadena);
		else
			System.out.print(cadena);
		
	}

}
