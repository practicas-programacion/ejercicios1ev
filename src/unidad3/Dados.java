package unidad3;

import java.util.Random;
import java.util.Scanner;

public class Dados {

	public static void main(String[] args) {
		
		int veces;
		
		Scanner in = new Scanner(System.in);
		Random cara = new Random();
		
		System.out.print("Dime cuantas veces lanzar el dado: ");
		veces = in.nextInt();
		
		int[] caras = new int[veces];
		
		for(int i = 0;i < veces; i++) {
			caras[i] = cara.nextInt(6)+1;
		}
		
		for(int i = 1; i <= 6; i++) {
			
			int counter = 0;
			
			for(int j = 0; j < caras.length ; j++){
				if(caras[j] == i)
					counter++;
			}
			
			System.out.printf("Número de veces que ha salido la cara %d = %d%n", i, counter);
		}		
		
		
		in.close();
		
	}


}
