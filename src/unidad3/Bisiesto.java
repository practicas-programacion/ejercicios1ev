package unidad3;

import java.util.Scanner;

public class Bisiesto {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		int ano;
		
		System.out.print("Escriba el año: ");
		ano = in.nextInt();
		
		if(ano % 4 == 0 && (ano % 100 != 0 || ano % 400 == 0))
			System.out.print("Es bisiesto");
		else
			System.out.print("No es bisiesto");
				
		in.close();

	}

}
