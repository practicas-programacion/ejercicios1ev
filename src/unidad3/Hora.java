package unidad3;

import java.util.Scanner;

public class Hora {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		int horas, minutos, segundos;
		
		System.out.print("Dime la hora: ");
		horas = in.nextInt();
		
		System.out.print("Dime los minutos: ");
		minutos = in.nextInt();
		
		System.out.print("Dime los segundos: ");
		segundos = in.nextInt();
		
		segundos++;
		
		if(segundos > 59) {
			segundos = 0;
			minutos++;
			if(minutos > 59) {
				minutos = 0;
				horas++;
			}
			if(horas > 23)
				horas = 0;
		}
		
		System.out.printf("La hora es %d:%d:%d%n", horas, minutos, segundos);
		
		in.close();

	}

}
