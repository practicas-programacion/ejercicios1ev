package unidad2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Cronometro {
	
	public static void main(String[] args) throws IOException {
		
		long timeBeforeAnswer, timeAfterAnswer;
		double time;
		String nombre;
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Dime tu nombre: ");
		timeBeforeAnswer = System.currentTimeMillis();
		nombre = in.readLine();
		timeAfterAnswer = System.currentTimeMillis();
		
		time = (timeAfterAnswer - timeBeforeAnswer) / 1000d;
		
		System.out.printf("Has tardardo %.3f segundos en introducir tu nombre %s",time,nombre);
		
	}

}
