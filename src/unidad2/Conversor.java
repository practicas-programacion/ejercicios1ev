package unidad2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Conversor {

	public static void main(String[] args) throws IOException {
		
		float dolares, cambio = 2.50f;
		String euros;
		
		BufferedReader reader =  
                new BufferedReader(new InputStreamReader(System.in)); 
		
		System.out.printf("Introduzca en número de euros (cambio = %.02f): ", cambio);
		
		euros = reader.readLine();
		
		dolares = Float.parseFloat(euros) * cambio;
		
		System.out.printf("El número de dólares de = %.2f", dolares);

	}

}
