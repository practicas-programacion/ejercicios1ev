package unidad2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Calificaciones {
	
	public static void main(String[] args) throws IOException {
		
		int matematicasPorcentajeExamen = 90, fisicaPorcentajeExamen = 80;
		int matematicasPorcentajeTareas = 10, fisicaPorcentajeTareas = 20;
		int matematicasTareas = 3, fisicaTareas = 2;
		// mNE = matemáticas nota examen
		float mNE, fNE;
		//mt1 = nota matemáticas tarea 1
		float mt1, mt2, mt3, ft1, ft2;
		// nFTM = nota final tareas mates
		float nFTM, nFTF, nFEM, nFEF;
		
		float notaFinalMates, notaFinalFisica, notaFinalCurso;
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.print("Nota E. Matemáticas: ");
		mNE = Float.parseFloat(in.readLine());
		
		System.out.print("Nota T1 Matemáticas: ");
		mt1 = Float.parseFloat(in.readLine());
		System.out.print("Nota T2 Matemáticas: ");
		mt2 = Float.parseFloat(in.readLine());
		System.out.print("Nota T3 Matemáticas: ");
		mt3 = Float.parseFloat(in.readLine());
		
		System.out.print("Nota E. Física: ");
		fNE = Float.parseFloat(in.readLine());
		
		System.out.print("Nota T1 Física: ");
		ft1 = Float.parseFloat(in.readLine());
		System.out.print("Nota T2 Física: ");
		ft2 = Float.parseFloat(in.readLine());
	
		nFEM = mNE * matematicasPorcentajeExamen / 100f;
		nFTM = (mt1+mt2+mt3)/3 * matematicasPorcentajeTareas / 100f;
		notaFinalMates = nFEM + nFTM;
		
		nFEF = fNE * fisicaPorcentajeExamen / 100f;
		nFTF = (ft1+ft2)/2 * fisicaPorcentajeTareas / 100f;
		notaFinalFisica = nFEF + nFTF;
		
		notaFinalCurso = (notaFinalMates + notaFinalFisica) /2;
		
		System.out.printf("La nota media en el examen de Mates es %.2f\n",notaFinalMates);
		System.out.printf("La nota media en el examen de Fisica es %.2f\n",notaFinalFisica);
		System.out.printf("La nota media en el examen de Curso es %.2f",notaFinalCurso);
		
		
		
	}

}
