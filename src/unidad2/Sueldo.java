package unidad2;

import java.util.Scanner;

public class Sueldo {

	public static void main(String[] args) {
		
		float sueldoBase, comision, ventas = 0;
		float venta = 1;
		
		Scanner in = new Scanner(System.in);
		
		System.out.print("Sueldo: ");
		sueldoBase = in.nextFloat();
		System.out.print("Comision: ");
		comision = in.nextFloat();
		
		while(venta != 0) {
			
			System.out.print("Venta: ");
			venta = in.nextFloat();
			
			ventas += venta; 
			
		}
		
		System.out.printf("El sueldo será de %.2f€ + el %.2f%% de comisión de los %.2f € de ventas. Total = %.2f",
				sueldoBase, comision, ventas, sueldoBase + (ventas * comision/100f));
		
	}

}
